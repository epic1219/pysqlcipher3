#!/bin/bash

# Get SQLCipher amalgamation files from git repository.

REPO="https://github.com/sqlcipher/sqlcipher.git"

if [ $# -gt 1 ]; then
    echo "Usage: bash ${0##*/} [VERSION]"
    exit 1
fi
VERSION=${1}
AMALGAMATION_DIR="`pwd`/src"

TEMP_DIR=`mktemp -d`
cd ${TEMP_DIR}
git clone ${REPO}
cd sqlcipher
if [ x"${VERSION}" = x ]; then
    VERSION=`git tag | tail -n 1`
fi
git checkout ${VERSION} && ./configure && make sqlite3.c

cp sqlite3.[ch] ${AMALGAMATION_DIR}/

cd ../.. && rm -rf ${TEMP_DIR}

sed -i 's!PYSQLITE_VERSION\([^"]*\)\"[0-9.]*\"!PYSQLITE_VERSION\1\"'${VERSION//v/}'\"!' ${AMALGAMATION_DIR}/module.h

echo "Done"
