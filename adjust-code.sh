#!/bin/bash

# Adjust codes necessarily.

sed -i -e '/^[^#]/d' lib/__init__.py
sed -i -e '/^$/d' lib/__init__.py
echo "" >> lib/__init__.py
echo "from .dbapi2 import *" >> lib/__init__.py

sed -i 's!^.*_sqlite.*$!from ._sqlite3 import *!' lib/dbapi2.py

sed -i 's!MODULE_NAME \"\([a-zA-Z]*\)\", !MODULE_NAME \".\1\",!' src/*

echo "Done"
