#!/usr/bin/env python
# -*- coding: utf-8 -*-

import glob
import os
import re

from setuptools import Extension
from setuptools import setup

PACKAGE_NAME = 'pysqlcipher3'
PACKAGE_VERSION = '2.6.10'

version_re = re.compile('#define PYSQLITE_VERSION "(.*)"')
with open(os.path.join('src', 'module.h')) as fh:
    for line in fh:
        match = version_re.match(line)
        if match:
            PACKAGE_VERSION = match.groups()[0]
            break

MODULE_NAME_POSTFIX = '.dbapi2'
EXTENSION_NAME_POSTFIX = '._sqlite3'

sources = glob.glob(os.path.join('src', '*.c'))
define_macros = [
    ('MODULE_NAME', f'"{PACKAGE_NAME + MODULE_NAME_POSTFIX}"'),
    ('SQLITE_TEMP_STORE', '2'),
    ('SQLITE_HAS_CODEC', '1')
]

setup(
    name=PACKAGE_NAME,
    version=PACKAGE_VERSION,
    packages=[PACKAGE_NAME],
    package_dir={PACKAGE_NAME: 'lib'},
    license='MIT',
    author='Jianzhi Li',
    author_email='jianzhi.lyy@gmail.com',
    description='Python3 DB-API 2.0 interface for SQLCipher',
    platforms='Linux',
    python_requires='>=3.0',
    ext_modules=[
        Extension(
            name=PACKAGE_NAME + EXTENSION_NAME_POSTFIX,
            sources=sources,
            define_macros=define_macros
        )
    ]
)
