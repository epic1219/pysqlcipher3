#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os

import requests

parser = argparse.ArgumentParser(
    description='Download needed sources from cpython github.')
parser.add_argument('version', help='Python version string, e.g. v3.6.10')
version = parser.parse_args().version

API = 'https://api.github.com'
RAW = 'https://raw.githubusercontent.com'

# download c extensions
os.makedirs('src', exist_ok=True)

res = requests.get(API + '/repos/python/cpython/contents/Modules',
                   params={'ref': version}).json()
res, = [r for r in res if r['name'] == '_sqlite']
res = requests.get(res['git_url']).json()['tree']

for fn in [r['path'] for r in res]:
    data = requests.get(RAW + f'/python/cpython/{version}/Modules/_sqlite/{fn}')
    with open(os.path.join('src', fn), 'w') as fh:
        fh.write(data.text)

# download python counterparts
os.makedirs('lib', exist_ok=True)

res = requests.get(API + '/repos/python/cpython/contents/Lib',
                   params={'ref': version}).json()
res, = [r for r in res if r['name'] == 'sqlite3']
res = requests.get(res['git_url']).json()['tree']

for fn in [r['path'] for r in res if r['type'] == 'blob']:
    data = requests.get(RAW + f'/python/cpython/{version}/Lib/sqlite3/{fn}')
    with open(os.path.join('lib', fn), 'w') as fh:
        fh.write(data.text)

print('Done')
