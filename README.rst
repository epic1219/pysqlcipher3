SQLCipher for Python3
=====================

Python3 DB-API 2.0 interface for SQLCipher.

Build
-----

Codes shipped with this repository are retrieved from
CPython 3.6.10 (2019-12-18),
SQLCipher 4.3.0 (2019-12-21),
corresponding SQLite 3.30.1 (2019-10-11).
You can build and install by execute::

  python setup.py bdist_wheel
  pip install dist/*.whl

If current configuration does not match your environment,
you can download and adjust specified version of code::

  python download-extension.py v3.6.10
  bash get-sqlcipher-amalgamation.sh v4.3.0
  bash adjust-code.sh

Also `setup.cfg` can be modified to control the compilation behavior,
see comments of `setup.cfg`. Among the options, it is recommended to
link against static openssl crypto library.

Usage
-----

You have to pass the ``PRAGMA key`` before doing any operations::

  import pysqlcipher3 as sqlite
  conn = sqlite.connect('test.db')
  c = conn.cursor()
  c.execute("PRAGMA key='test'")
  c.execute('''create table stocks (date text, trans text, symbol text, qty real, price real)''')
  c.execute("""insert into stocks values ('2006-01-05', 'BUY', 'HAT', 100, 35.14)""")
  conn.commit()
  c.close()

You can quickly verify that your database file is indeed encrypted::

  hexdump -C test.db | head -n 5
  00000000  cf 30 cc b2 02 c9 57 82  a2 a1 12 ef 63 37 0d 5e  |.0....W.....c7.^|
  00000010  07 6e bd 82 b7 96 e2 19  41 26 da 84 7c 62 ac 5f  |.n......A&..|b._|
  00000020  86 a2 a3 dc 9b 6d f0 e8  0b e9 ad fa a6 a2 16 04  |.....m..........|
  00000030  75 58 01 31 d9 e8 53 a9  0c c8 1c d9 8b 91 19 2c  |uX.1..S........,|
  00000040  31 6b 2b 80 d0 d0 9a 66  f4 20 97 c9 72 0e b9 d2  |1k+....f. ..r...|
